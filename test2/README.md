# 曹海鑫 202010414301 软件工程3班
## 实验2：用户及权限管理
### 实验目的
掌握用户管理、角色管理、权根维护与分配的能力，掌握用户之间共享对象的操作技能，以及概要文件对用户的限制。
### 实验内容
Oracle有一个开发者角色resource，可以创建表、过程、触发器等对象，但是不能创建视图。本训练要求：

在pdborcl插接式数据中创建一个新的本地角色con_res_role，该角色包含connect和resource角色，同时也包含CREATE VIEW权限，这样任何拥有con_res_role的用户就同时拥有这三种权限。
创建角色之后，再创建用户sale，给用户分配表空间，设置限额为50M，授予con_res_role角色。
最后测试：用新用户sale连接数据库、创建表，插入数据，创建视图，查询表和视图的数据。
### 实验参考步骤
对于以下的对象名称con_res_role，sale，在实验的时候应该修改为自己的名称。

第1步：以system登录到pdborcl，创建角色con_res_role和用户sale，并授权和分配空间：
```
[oracle@oracle1 ~]$ sqlplus system/123@pdborcl

SQL*Plus: Release 12.2.0.1.0 Production on 星期二 4月 18 16:52:37 2023

Copyright (c) 1982, 2016, Oracle.  All rights reserved.

上次成功登录时间: 星期二 4月  18 2023 16:47:52 +08:00

连接到: 
Oracle Database 12c Enterprise Edition Release 12.2.0.1.0 - 64bit Production

CREATE ROLE con_res_role;
GRANT connect,resource,CREATE VIEW TO con_res_role;
CREATE USER sale IDENTIFIED BY 123 DEFAULT TABLESPACE users TEMPORARY TABLESPACE temp;
ALTER USER sale default TABLESPACE "USERS";
ALTER USER sale QUOTA 50M ON users;
GRANT con_res_role TO sale;

角色已创建。

SQL> 
授权成功。

SQL> CREATE USER sale IDENTIFIED BY 123 DEFAULT TABLESPACE users TEMPORARY TABLESPACE temp
            *
第 1 行出现错误:
ORA-01920: 用户名 'SALE' 与另外一个用户名或角色名发生冲突


SQL> 
用户已更改。

SQL> 
用户已更改。

SQL> 
授权成功。

SQL> 

```
> 语句“ALTER USER sale QUOTA 50M ON users;”是指授权sale用户访问users表空间，空间限额是50M。

- 第2步：新用户sale连接到pdborcl，创建表customers和视图customers_view，插入数据，最后将customers_view的SELECT对象权限授予hr用户。session_privs，session_roles可以查看会话权限和角色。
```
[oracle@oracle1 ~]$ sqlplus sale/123@pdborcl

SQL*Plus: Release 12.2.0.1.0 Production on 星期二 4月 18 16:53:43 2023

Copyright (c) 1982, 2016, Oracle.  All rights reserved.

上次成功登录时间: 星期二 4月  18 2023 16:30:55 +08:00

连接到: 
Oracle Database 12c Enterprise Edition Release 12.2.0.1.0 - 64bit Production

SQL> show user;
USER 为 "SALE"
SELECT * FROM session_privs;
SELECT * FROM session_roles;

PRIVILEGE
----------------------------------------
CREATE SESSION
CREATE TABLE
CREATE CLUSTER
CREATE VIEW
CREATE SEQUENCE
CREATE PROCEDURE
CREATE TRIGGER
CREATE TYPE
CREATE OPERATOR
CREATE INDEXTYPE
SET CONTAINER

已选择 11 行。

SQL> 
ROLE
--------------------------------------------------------------------------------
CON_RES_ROLE
CONNECT
RESOURCE
SODA_APP

CREATE TABLE customers (id number,name varchar(50)) TABLESPACE "USERS" ;
INSERT INTO customers(id,name)VALUES(1,'zhang');
INSERT INTO customers(id,name)VALUES (2,'wang');
CREATE VIEW customers_view AS SELECT name FROM customers;
GRANT SELECT ON customers_view TO hr;
SELECT * FROM customers_view;
CREATE TABLE customers (id number,name varchar(50)) TABLESPACE "USERS"
             *
第 1 行出现错误:
ORA-00955: 名称已由现有对象使用


SQL> 
已创建 1 行。

SQL> 
已创建 1 行。

SQL> CREATE VIEW customers_view AS SELECT name FROM customers
            *
第 1 行出现错误:
ORA-00955: 名称已由现有对象使用


SQL> 
授权成功。

SQL> 
NAME
--------------------------------------------------
zhang
wang
zhang
wang

SQL> 

```
第3步：用户hr连接到pdborcl，查询sale授予它的视图customers_view
```
[oracle@oracle1 ~]$ sqlplus hr/123@pdborcl

SQL*Plus: Release 12.2.0.1.0 Production on 星期二 4月 18 16:56:28 2023

Copyright (c) 1982, 2016, Oracle.  All rights reserved.

上次成功登录时间: 星期二 4月  18 2023 16:55:47 +08:00

连接到: 
Oracle Database 12c Enterprise Edition Release 12.2.0.1.0 - 64bit Production

SQL> SELECT * FROM sale.customers_view;

NAME
--------------------------------------------------
zhang
wang
zhang
wang

SQL> 

```
测试一下用户hr,sale之间的表的共享，只读共享和读写共享都测试一下。
sale用户只共享了视图customers_view给hr,没有共享表customers。所以hr无法查询表customers。
### 概要文件设置,用户最多登录时最多只能错误3次
```
[oracle@oracle1 ~]$ sqlplus system/123@pdborcl

SQL*Plus: Release 12.2.0.1.0 Production on 星期二 4月 18 16:57:10 2023

Copyright (c) 1982, 2016, Oracle.  All rights reserved.

上次成功登录时间: 星期二 4月  18 2023 16:52:40 +08:00

连接到: 
Oracle Database 12c Enterprise Edition Release 12.2.0.1.0 - 64bit Production

SQL> ALTER PROFILE default LIMIT FAILED_LOGIN_ATTEMPTS 3;

配置文件已更改

SQL> 

```
- 设置后，sqlplus hr/错误密码@pdborcl 。3次错误密码后，用户被锁定。
- 锁定后，通过system用户登录，alter user sale unlock命令解锁。
```
 [oracle@oracle1 ~]$ sqlplus system/123@pdborcl

SQL*Plus: Release 12.2.0.1.0 Production on 星期二 4月 18 16:58:30 2023

Copyright (c) 1982, 2016, Oracle.  All rights reserved.

上次成功登录时间: 星期二 4月  18 2023 16:57:10 +08:00

连接到: 
Oracle Database 12c Enterprise Edition Release 12.2.0.1.0 - 64bit Production

SQL> alter user sale  account unlock;

用户已更改。

SQL> 

```
### 数据库和表空间占用分析

当实验做完之后，数据库pdborcl中包含了新的角色con_res_role和用户sale。
新用户sale使用默认表空间users存储表的数据。
随着用户往表中插入数据，表空间的磁盘使用量会增加
### 查看数据库的使用情况

以下样例查看表空间的数据库文件，以及每个文件的磁盘占用情况。
```
[oracle@oracle1 ~]$ sqlplus system/123@pdborcl

SQL*Plus: Release 12.2.0.1.0 Production on 星期二 4月 18 16:59:12 2023

Copyright (c) 1982, 2016, Oracle.  All rights reserved.

上次成功登录时间: 星期二 4月  18 2023 16:58:30 +08:00

连接到: 
Oracle Database 12c Enterprise Edition Release 12.2.0.1.0 - 64bit Production

SQL> SELECT tablespace_name,FILE_NAME,BYTES/1024/1024 MB,MAXBYTES/1024/1024 MAX_MB,autoextensible FROM dba_data_files  WHERE  tablespace_name='USERS';

TABLESPACE_NAME
------------------------------
FILE_NAME
--------------------------------------------------------------------------------
	MB     MAX_MB AUT
---------- ---------- ---
USERS
/home/oracle/app/oracle/oradata/orcl/pdborcl/users01.dbf
	 5 32767.9844 YES


SELECT a.tablespace_name "表空间名",Total/1024/1024 "大小MB",
 free/1024/1024 "剩余MB",( total - free )/1024/1024 "使用MB",
 Round(( total - free )/ total,4)* 100 "使用率%"
 from (SELECT tablespace_name,Sum(bytes)free
        FROM   dba_free_space group  BY tablespace_name)a,
       (SELECT tablespace_name,Sum(bytes)total FROM dba_data_files
        group  BY tablespace_name)b
  8   where  a.tablespace_name = b.tablespace_name;

表空间名                           大小MB     剩余MB     使用MB    使用率%
------------------------------ ---------- ---------- ---------- ----------
SYSAUX				      440	96.5	  343.5      78.07
UNDOTBS1			      100	  33	     67 	67
USERS					5     3.9375	 1.0625      21.25
SYSTEM				      260     9.0625   250.9375      96.51

SQL> 

```
- autoextensible是显示表空间中的数据文件是否自动增加。
- MAX_MB是指数据文件的最大容量。

### 实验结束删除用户和角色
```
[oracle@oracle1 ~]$ sqlplus system/123@pdborcl

SQL*Plus: Release 12.2.0.1.0 Production on 星期二 4月 18 17:00:48 2023

Copyright (c) 1982, 2016, Oracle.  All rights reserved.

上次成功登录时间: 星期二 4月  18 2023 16:59:13 +08:00

连接到: 
Oracle Database 12c Enterprise Edition Release 12.2.0.1.0 - 64bit Production

drop role con_res_role;
drop user sale cascade;

角色已删除。

SQL> drop user sale cascade
*
第 1 行出现错误:
ORA-01940: 无法删除当前连接的用户


SQL> 

```
## 结论

我们在本实验中成功掌握了Oracle中的用户管理、角色管理和权限维护与分配的技能，并学会了操作用户共享对象。实验结论如下：

成功创建了本地角色con_res_role，该角色包含connect和resource角色，并授予了CREATE VIEW权限。任何拥有con_res_role的用户都将同时具备这三种权限。

成功创建了用户sale，并为其分配了表空间，并将限额设置为50M，并授予了con_res_role角色。

成功使用用户sale连接数据库，创建表，插入数据，创建视图，并查询表和视图的数据，表明用户sale具有connect、resource和CREATE VIEW权限。

通过本实验，我们达到了目的，成功掌握了用户管理、角色管理和权限维护与分配的能力，以及用户之间共享对象的操作技能。