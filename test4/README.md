# 实验4：PL/SQL语言打印杨辉三角
 姓名：曹海鑫          学号：202010414301         班级：3班

## 实验目的

掌握Oracle PL/SQL语言以及存储过程的编写。

## 实验内容

- 认真阅读并运行下面的杨辉三角源代码。
- 将源代码转为hr用户下的一个存储过程Procedure，名称为YHTriange，存储起来。存储过程接受行数N作为参数。
- 运行这个存储过程即可以打印出N行杨辉三角。
- 写出创建YHTriange的SQL语句。

## 杨辉三角源代码

```
set serveroutput on;
declare
type t_number is varray (100) of integer not null; --数组
i integer;
j integer;
spaces varchar2(30) :='   '; --三个空格，用于打印时分隔数字
N integer := 9; -- 一共打印9行数字
rowArray t_number := t_number();
begin
    dbms_output.put_line('1'); --先打印第1行
    dbms_output.put(rpad(1,9,' '));--先打印第2行
    dbms_output.put(rpad(1,9,' '));--打印第一个1
    dbms_output.put_line(''); --打印换行
    --初始化数组数据
    for i in 1 .. N loop
        rowArray.extend;
    end loop;
    rowArray(1):=1;
    rowArray(2):=1;    
    for i in 3 .. N --打印每行，从第3行起
    loop
        rowArray(i):=1;    
        j:=i-1;
            --准备第j行的数组数据，第j行数据有j个数字，第1和第j个数字为1
            --这里从第j-1个数字循环到第2个数字，顺序是从右到左
        while j>1 
        loop
            rowArray(j):=rowArray(j)+rowArray(j-1);
            j:=j-1;
        end loop;
            --打印第i行
        for j in 1 .. i
        loop
            dbms_output.put(rpad(rowArray(j),9,' '));--打印第一个1
        end loop;
        dbms_output.put_line(''); --打印换行
    end loop;
END;
/
```



创建存储过程：

```
create or replace PROCEDURE YHTRLANGLE(p_rows IN INTEGER) AS
type t_number is varray (100) of integer not null;
i integer;
j integer;
spaces varchar2(30) :='   '; 
N integer := p_rows; 
rowArray t_number := t_number();
begin
    dbms_output.put_line('1'); 
    dbms_output.put(rpad(1,9,' '));
    dbms_output.put(rpad(1,9,' '));
    dbms_output.put_line(''); 
   
    for i in 1 .. N loop
        rowArray.extend;
    end loop;
    rowArray(1):=1;
    rowArray(2):=1;    
    for i in 3 .. N 
    loop
        rowArray(i):=1;    
        j:=i-1;
           
        while j>1 
        loop
            rowArray(j):=rowArray(j)+rowArray(j-1);
            j:=j-1;
        end loop;
            --打印第i行
        for j in 1 .. i
        loop
            dbms_output.put(rpad(rowArray(j),9,' '));
        end loop;
        dbms_output.put_line(''); 
    end loop;
END;
```



在hr用户进行存储过程的调用：

```
set SERVEROUTPUT on
begin
YHTRLANGLE(5);
end;·
```
# 实验总结：
本次实验通过实际的例子理解了Oracle PL/SQL语言的基本编写方式和存储过程的使用。存储过程是一种数据库对象，它可以封装一系列的SQL语句和业务逻辑，并通过参数进行灵活的数据操作和处理。通过将源代码转换为存储过程，可以提高代码的复用性和可维护性，并且方便进行调用和管理。

在编写存储过程时，需要注意以下几点：

- 定义存储过程时需要指定输入参数和输出参数，以便传递和返回数据。
- 使用PL/SQL语言编写存储过程的具体逻辑，包括变量的声明和赋值、循环语句的使用、条件语句的使用等。
- 考虑存储过程的健壮性和可维护性，确保程序的正确性和稳定性。
- 使用SQL语句进行数据库的操作，包括查询数据和更新数据等操作。

通过本次实验，更加深入地了解Oracle PL/SQL语言的使用，掌握存储过程的编写方式，并在实际的开发中灵活应用相关知识。存储过程的使用可以提高数据库的性能和效率，同时简化开发工作，提升开发效率。