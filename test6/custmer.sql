prompt PL/SQL Developer Export User Objects for user AH@ORCL
prompt Created by jiang on 2023年5月25日
set define off
spool custmer.log

prompt
prompt Creating table CUSTOMER
prompt =======================
prompt
create table AH.CUSTOMER
(
  customer_id   VARCHAR2(64) not null,
  customer_name VARCHAR2(256),
  email         VARCHAR2(128)
)
tablespace DATE_TABLESPACE
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
alter table AH.CUSTOMER
  add constraint CUSTOMER_KAY_ID primary key (CUSTOMER_ID)
  using index 
  tablespace DATE_TABLESPACE
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );

prompt
prompt Creating table ORDERS
prompt =====================
prompt
create table AH.ORDERS
(
  order_id   VARCHAR2(64) not null,
  order_date DATE,
  courst_id  VARCHAR2(64)
)
tablespace DATE_TABLESPACE
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
alter table AH.ORDERS
  add constraint ORDERS_KAY_ID primary key (ORDER_ID)
  using index 
  tablespace DATE_TABLESPACE
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );

prompt
prompt Creating table ORDERTETAIL
prompt ==========================
prompt
create table AH.ORDERTETAIL
(
  order_id   VARCHAR2(64) not null,
  product_id VARCHAR2(64) not null,
  quantity   NUMBER,
  price      NUMBER(5,2)
)
tablespace DATE_TABLESPACE
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
alter table AH.ORDERTETAIL
  add constraint ORDERTETAIL_KAY_ID primary key (ORDER_ID, PRODUCT_ID)
  using index 
  tablespace DATE_TABLESPACE
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );

prompt
prompt Creating table PRODUCTS
prompt =======================
prompt
create table AH.PRODUCTS
(
  product_id   VARCHAR2(64) not null,
  product_name VARCHAR2(256),
  price        NUMBER(5,2),
  quantity     NUMBER
)
tablespace DATE_TABLESPACE
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
alter table AH.PRODUCTS
  add constraint PRODUCTS_KAY_ID primary key (PRODUCT_ID)
  using index 
  tablespace DATE_TABLESPACE
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );

prompt
prompt Creating package CUST_PKG
prompt =========================
prompt
create or replace package ah.cust_pkg is
  procedure pro_cust_order(cust_no in varchar2) ;

end cust_pkg;
/

prompt
prompt Creating package body CUST_PKG
prompt ==============================
prompt
create or replace package body ah.cust_pkg is

  procedure  pro_cust_order(cust_no in varchar2)
     is
V_CUSTOMER_NAME varchar2(64);  
V_EMAIL varchar2(128); 
V_ORDER_DATE date; 
V_PRODUCT_NAME varchar2(128); 
V_PRICE NUMBER;
V_QUANTITY  NUMBER; 
V_ordersum  NUMBER;
flag1 number;   
p_cur SYS_REFCURSOR;
     begin
       
     select length(cust_no) into flag1 from dual;
     
    if flag1<1 then
      dbms_output.put_line('客户号不能为空');
      return ;
     end if; 
     
  OPEN p_cur FOR 
     select t3.CUSTOMER_NAME,t3.EMAIL, t1.ORDER_DATE , t2.PRODUCT_NAME ,t2.PRICE  ,t2.QUANTITY ,
     t2.ordersum 
     from 
     (select  ORDER_ID, ORDER_DATE, COURST_ID  from orders where COURST_ID=cust_no)t1
     left join
     (
     select  
         x1.ORDER_ID,
        (QUANTITY * PRICE) as ordersum,
        (select PRODUCT_NAME FROM products X2 WHERE X2.PRODUCT_ID=X1.PRODUCT_ID) AS PRODUCT_NAME ,
        (select PRICE FROM products X2 WHERE X2.PRODUCT_ID=X1.PRODUCT_ID) AS PRICE ,
        (select QUANTITY FROM products X2 WHERE X2.PRODUCT_ID=X1.PRODUCT_ID) AS QUANTITY 
     FROM ordertetail x1)t2 on t1.ORDER_ID=t2.ORDER_ID
     
     left join
     (select  CUSTOMER_ID,CUSTOMER_NAME,EMAIL FROM  CUSTOMER where CUSTOMER_ID= cust_no)t3 on t1.COURST_ID=t3.CUSTOMER_ID ;
     
  LOOP 
    FETCH p_cur
    INTO  V_CUSTOMER_NAME, V_EMAIL , V_ORDER_DATE ,V_PRODUCT_NAME ,V_QUANTITY ,V_PRICE,V_ordersum ;
    EXIT WHEN p_cur%NOTFOUND;
    DBMS_OUTPUT.PUT_LINE(V_CUSTOMER_NAME||' =='||V_EMAIL ||'== '|| V_ORDER_DATE ||' =='||V_PRODUCT_NAME ||'== '||V_QUANTITY ||' =='||V_PRICE ||'== '||V_ordersum);
  END LOOP;


 CLOSE p_cur;
end pro_cust_order;
end cust_pkg;
/


prompt Done
spool off
set define on
