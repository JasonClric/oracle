﻿﻿# 实验6（期末考核） 基于Oracle数据库的商品销售系统的设计 

- 学号： 202010414301   姓名：曹海鑫

### 实验目的

- 设计一套基于Oracle数据库的商品销售系统的数据库设计方案。
  - 表及表空间设计方案。至少两个表空间，至少4张表，总的模拟数据量不少于10万条。
  - 设计权限及用户分配方案。至少两个用户。
  - 在数据库中建立一个程序包，在包中用PL/SQL语言设计一些存储过程和函数，实现比较复杂的业务逻辑。
  - 设计一套数据库的备份方案。

### 期末考核要求

- 实验在自己的计算机上完成。
- 文档`必须提交`到你的oracle项目中的test6目录中。test6目录中必须至少有3个文件：
  - test6.md主文件。
  - 数据库创建和维护用的脚本文件*.sql。
  - [test6_design.docx](./test6_design.docx)，学校格式的完整报告。
- 文档中所有设计和数据都必须是独立完成的真实实验结果。不得抄袭，杜撰。
- 提交时间： 2023-5-26日前

### 评分标准

| 评分项     | 评分标准                             | 满分 |
| :--------- | :----------------------------------- | :--- |
| 文档整体   | 文档内容详实、规范，美观大方         | 10   |
| 表设计     | 表设计及表空间设计合理，样例数据合理 | 20   |
| 用户管理   | 权限及用户分配方案设计正确           | 20   |
| PL/SQL设计 | 存储过程和函数设计正确               | 30   |
| 备份方案   | 备份方案设计正确                     | 20   |

### 步骤

## 一、表设计

### 1.1 创建表空间

```

C:\Users\jiang>sqlplus sys/123456 as sysdba;

SQL*Plus: Release 11.2.0.1.0 Production on 星期三 5月 24 13:48:45 2023

Copyright (c) 1982, 2010, Oracle.  All rights reserved.


连接到:
Oracle Database 11g Release 11.2.0.1.0 - 64bit Production
SQL> create tablespace date_tablespace datafile 'D:\app\jiang\oradata\orcl\date_tablespace.dbf'size 1024M autoextend on next 10M;

表空间已创建。

SQL> create tablespace index_tablespace datafile 'D:\app\jiang\oradata\orcl\index_tablespace.dbf'size 1024M autoextend on next 10M;

表空间已创建。

SQL>


```

![](1创建表空间（date_tablespace）.jpg)

### 1.2 创建产品表

```
-- Create table
create table PRODUCTS
(
  product_id   VARCHAR2(64) not null,
  product_name VARCHAR2(256),
  price        NUMBER(5,2),
  quantity     NUMBER
)
tablespace DATE_TABLESPACE
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
-- Create/Recreate primary, unique and foreign key constraints 
alter table PRODUCTS
  add constraint PRODUCTS_KAY_ID primary key (PRODUCT_ID)
  using index 
  tablespace DATE_TABLESPACE
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
-- Grant/Revoke object privileges 
grant select on PRODUCTS to NORMAL_CUST;


```

![](2产品信息表.jpg)

### 1.3 创建客户表

```
-- Create table
create table CUSTOMER
(
  customer_id   VARCHAR2(64) not null,
  customer_name VARCHAR2(256),
  email         VARCHAR2(128)
)
tablespace DATE_TABLESPACE
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
-- Create/Recreate primary, unique and foreign key constraints 
alter table CUSTOMER
  add constraint CUSTOMER_KAY_ID primary key (CUSTOMER_ID)
  using index 
  tablespace DATE_TABLESPACE
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
-- Grant/Revoke object privileges 
grant select on CUSTOMER to NORMAL_CUST;

```

![](3客户信息表.jpg)

### **1.4 创建订单表**

```
-- Create table
create table ORDERS
(
  order_id   VARCHAR2(64) not null,
  order_date DATE,
  courst_id  VARCHAR2(64)
)
tablespace DATE_TABLESPACE
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
-- Create/Recreate primary, unique and foreign key constraints 
alter table ORDERS
  add constraint ORDERS_KAY_ID primary key (ORDER_ID)
  using index 
  tablespace DATE_TABLESPACE
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
-- Grant/Revoke object privileges 
grant select on ORDERS to NORMAL_CUST;

```

![](4订单表.jpg)

### **1.5 创建订单详情表**

```
create table SCORE(
cours_id varchar2(64),
student_id varchar2(64),
grade number(3,2)

)

tablespace date_tablespace
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );

  
alter table SCORE
  add constraint SCORE_KAY_ID primary key (cours_id,student_id)
  using index 
  tablespace date_tablespace
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
```

 ![](6订单详情表.jpg)

### **1.6 插入产品表数据**

```
BEGIN
FOR i IN 1..1000 LOOP
INSERT INTO products (PRODUCT_ID, PRODUCT_NAME,PRICE,QUANTITY)
SELECT i AS DEPT_ID, 'PRODUCT_NAME'||i AS PRODUCT_NAME,
substr(DBMS_RANDOM.VALUE(1,100),1,4)  as PRICE,
ceil(DBMS_RANDOM.VALUE(1,100)) as QUANTITY

FROM dual
WHERE NOT EXISTS (SELECT i FROM products WHERE PRODUCT_ID = i);
END LOOP;
COMMIT;
END;
```

![](7插入产品数据.jpg)



### **1.7 插入客户表数据**

```
BEGIN
FOR i IN 1..30000 LOOP
INSERT INTO ORDER_ID (CUSTOMER_ID, CUSTOMER_NAME,EMAIL)
SELECT i AS CUSTOMER_ID,
 'CUSTOMER_NAME'||i AS CUSTOMER_NAME,
 'CUSTOMER'||i||'@example.com' as EMAIL
FROM dual
WHERE NOT EXISTS (SELECT i FROM CUSTOMER WHERE CUSTOMER_ID = i);
END LOOP;
COMMIT;
END;
```

![](8客户表.jpg)

### 1.8  插入订单表

```
declare
flag1 varchar2(4000) ;
flag2 varchar2(4000) ;

BEGIN
FOR i IN 1..60000 LOOP
  
  select ceil(DBMS_RANDOM.VALUE(1,30000)) into flag1 from dual;
  select ceil(DBMS_RANDOM.VALUE(1,360)) into flag2 from dual;

    INSERT INTO ORDERs (ORDER_ID,ORDER_DATE,COURST_ID) VALUES ( i,  sysdate - flag2 || '','' || flag1 || '');

if MOD(i,1000)=0
  then 
    commit;
end if;
END LOOP;
COMMIT;
END;

```

![](9订单表.jpg)

### 1.9 订单详情表

```
declare
flag1 varchar2(4000) ;
flag2 varchar2(4000) ;
flag3 number ;
flag4 number ;
flag5 number ;
BEGIN
FOR i IN 1..60000 LOOP
  select ceil(DBMS_RANDOM.VALUE(1,30000)) into flag1 from dual;
  select ceil(DBMS_RANDOM.VALUE(1,1000)) into flag2 from dual;
   select ceil(DBMS_RANDOM.VALUE(1,100)) into flag3  from dual ;
   select substr(DBMS_RANDOM.VALUE(1,100),1,4) into flag5  from dual ;

  select count(1) into flag4 from ordertetail t where t.PRODUCT_ID=flag2 and t.ORDER_ID=flag1 ;
  if flag4>0 
    then
        update ordertetail set PRODUCT_ID=ceil(DBMS_RANDOM.VALUE(2,999))+1  ,ORDER_ID= ceil(DBMS_RANDOM.VALUE(1,30000)) 
          where PRODUCT_ID=flag2 and ORDER_ID=flag1 ;
  end if;
    INSERT INTO ordertetail (ORDER_ID,PRODUCT_ID,QUANTITY,PRICE) VALUES ('' || flag1 || '','' || flag2 || '','' || flag3 || '','' || flag5 || '');
     commit;

END LOOP;
COMMIT;
END;


```

![](10订单详情.jpg)



## 二 、用户管理

### **2.1 新建用户以及授权**

```
 CREATE USER normal_cust PROFILE DEFAULT IDENTIFIED BY 123456 
 DEFAULT TABLESPACE index_tablespace TEMPORARY TABLESPACE TEMP ACCOUNT UNLOCK;
 
 CREATE USER admin_cust PROFILE DEFAULT IDENTIFIED BY 123456 
 DEFAULT TABLESPACE index_tablespace TEMPORARY TABLESPACE TEMP ACCOUNT UNLOCK;
 
GRANT SELECT  ON products TO normal_cust;
GRANT SELECT  ON customer TO normal_cust;
GRANT SELECT  ON orders TO normal_cust;
GRANT SELECT  ON ordertetail TO normal_cust;
GRANT dba to admin_cust ;


```

![](11新建用户.jpg)



## 三、   PL/SQL设计  

###  3.1 设计存储过程

```

# 包结构
create or replace package cust_pkg is
  procedure pro_cust_order(cust_no in varchar2) ;
end cust_pkg;


# 具体存储过程
create or replace package body cust_pkg is

  procedure  pro_cust_order(cust_no in varchar2)
     is
V_CUSTOMER_NAME varchar2(64);  
V_EMAIL varchar2(128); 
V_ORDER_DATE date; 
V_PRODUCT_NAME varchar2(128); 
V_PRICE NUMBER;
V_QUANTITY  NUMBER; 
V_ordersum  NUMBER;
flag1 number;   
p_cur SYS_REFCURSOR;
     begin
       
     select length(cust_no) into flag1 from dual;
     
    if flag1<1 then
      dbms_output.put_line('客户号不能为空');
      return ;
     end if; 
     
  OPEN p_cur FOR 
     select t3.CUSTOMER_NAME,t3.EMAIL, t1.ORDER_DATE , t2.PRODUCT_NAME ,t2.PRICE  ,t2.QUANTITY ,
     t2.ordersum 
     from 
     (select  ORDER_ID, ORDER_DATE, COURST_ID  from orders where COURST_ID=cust_no)t1
     left join
     (
     select  
         x1.ORDER_ID,
        (QUANTITY * PRICE) as ordersum,
        (select PRODUCT_NAME FROM products X2 WHERE X2.PRODUCT_ID=X1.PRODUCT_ID) AS PRODUCT_NAME ,
        (select PRICE FROM products X2 WHERE X2.PRODUCT_ID=X1.PRODUCT_ID) AS PRICE ,
        (select QUANTITY FROM products X2 WHERE X2.PRODUCT_ID=X1.PRODUCT_ID) AS QUANTITY 
     FROM ordertetail x1)t2 on t1.ORDER_ID=t2.ORDER_ID
     
     left join
     (select  CUSTOMER_ID,CUSTOMER_NAME,EMAIL FROM  CUSTOMER where CUSTOMER_ID= cust_no)t3 on t1.COURST_ID=t3.CUSTOMER_ID ;
     
  LOOP 
    FETCH p_cur
    INTO  V_CUSTOMER_NAME, V_EMAIL , V_ORDER_DATE ,V_PRODUCT_NAME ,V_QUANTITY ,V_PRICE,V_ordersum ;
    EXIT WHEN p_cur%NOTFOUND;
    DBMS_OUTPUT.PUT_LINE(V_CUSTOMER_NAME||' =='||V_EMAIL ||'== '|| V_ORDER_DATE ||' =='||V_PRODUCT_NAME ||'== '||V_QUANTITY ||' =='||V_PRICE ||'== '||V_ordersum);
  END LOOP;
 CLOSE p_cur;
end pro_cust_order;
end cust_pkg;

```

![](11创建存储过程.jpg)

![](13存储过程执行结果.jpg)

## **四 、设计备份方案**

### 4.1 数据备份

```
C:\Users\jiang>rman target sys/123456 catalog rman/rman@rmanconn

恢复管理器: Release 11.2.0.1.0 - Production on 星期四 5月 25 18:46:21 2023

Copyright (c) 1982, 2009, Oracle and/or its affiliates.  All rights reserved.

连接到目标数据库: ORCL (DBID=1662702591)
连接到恢复目录数据库

RMAN> backup as  copy database format 'D:\oracle\bakup\rmanbak\db\full_db_%d%u.bak' ;

启动 backup 于 25-5月 -23
正在启动全部恢复目录的 resync
完成全部 resync
分配的通道: ORA_DISK_1
通道 ORA_DISK_1: SID=6 设备类型=DISK
通道 ORA_DISK_1: 启动数据文件副本
输入数据文件: 文件号=00003 名称=D:\APP\JIANG\ORADATA\ORCL\UNDOTBS01.DBF
输出文件名=D:\ORACLE\BAKUP\RMANBAK\DB\FULL_DB_ORCLBC1T2C17.BAK 标记=TAG20230525T184703 RECID=47 STAMP=1137782828
通道 ORA_DISK_1: 数据文件复制完毕, 经过时间: 00:00:07
通道 ORA_DISK_1: 启动数据文件副本
输入数据文件: 文件号=00006 名称=D:\APP\JIANG\ORADATA\ORCL\AH.DBF
输出文件名=D:\ORACLE\BAKUP\RMANBAK\DB\FULL_DB_ORCLBD1T2C1E.BAK 标记=TAG20230525T184703 RECID=48 STAMP=1137782832
通道 ORA_DISK_1: 数据文件复制完毕, 经过时间: 00:00:03
通道 ORA_DISK_1: 启动数据文件副本
输入数据文件: 文件号=00007 名称=D:\APP\JIANG\ORADATA\RMANSJWJ\TBS_RMAN.DBF
输出文件名=D:\ORACLE\BAKUP\RMANBAK\DB\FULL_DB_ORCLBE1T2C1H.BAK 标记=TAG20230525T184703 RECID=49 STAMP=1137782835
通道 ORA_DISK_1: 数据文件复制完毕, 经过时间: 00:00:03
通道 ORA_DISK_1: 启动数据文件副本
输入数据文件: 文件号=00008 名称=D:\APP\JIANG\ORADATA\ORCL\DATE_TABLESPACE.DBF
输出文件名=D:\ORACLE\BAKUP\RMANBAK\DB\FULL_DB_ORCLBF1T2C1K.BAK 标记=TAG20230525T184703 RECID=50 STAMP=1137782837
通道 ORA_DISK_1: 数据文件复制完毕, 经过时间: 00:00:03
通道 ORA_DISK_1: 启动数据文件副本
输入数据文件: 文件号=00009 名称=D:\APP\JIANG\ORADATA\ORCL\INDEX_TABLESPACE.DBF
输出文件名=D:\ORACLE\BAKUP\RMANBAK\DB\FULL_DB_ORCLBG1T2C1N.BAK 标记=TAG20230525T184703 RECID=51 STAMP=1137782840
通道 ORA_DISK_1: 数据文件复制完毕, 经过时间: 00:00:03
通道 ORA_DISK_1: 启动数据文件副本
输入数据文件: 文件号=00001 名称=D:\APP\JIANG\ORADATA\ORCL\SYSTEM01.DBF
输出文件名=D:\ORACLE\BAKUP\RMANBAK\DB\FULL_DB_ORCLBH1T2C1Q.BAK 标记=TAG20230525T184703 RECID=52 STAMP=1137782859
通道 ORA_DISK_1: 数据文件复制完毕, 经过时间: 00:00:25
通道 ORA_DISK_1: 启动数据文件副本
输入数据文件: 文件号=00002 名称=D:\APP\JIANG\ORADATA\ORCL\SYSAUX01.DBF
输出文件名=D:\ORACLE\BAKUP\RMANBAK\DB\FULL_DB_ORCLBI1T2C2J.BAK 标记=TAG20230525T184703 RECID=53 STAMP=1137782869
通道 ORA_DISK_1: 数据文件复制完毕, 经过时间: 00:00:03
通道 ORA_DISK_1: 启动数据文件副本
输入数据文件: 文件号=00005 名称=D:\APP\JIANG\ORADATA\ORCL\EXAMPLE01.DBF
输出文件名=D:\ORACLE\BAKUP\RMANBAK\DB\FULL_DB_ORCLBJ1T2C2M.BAK 标记=TAG20230525T184703 RECID=54 STAMP=1137782871
通道 ORA_DISK_1: 数据文件复制完毕, 经过时间: 00:00:01
通道 ORA_DISK_1: 启动数据文件副本
输入数据文件: 文件号=00004 名称=D:\APP\JIANG\ORADATA\ORCL\USERS01.DBF
输出文件名=D:\ORACLE\BAKUP\RMANBAK\DB\FULL_DB_ORCLBK1T2C2N.BAK 标记=TAG20230525T184703 RECID=55 STAMP=1137782872
通道 ORA_DISK_1: 数据文件复制完毕, 经过时间: 00:00:02
完成 backup 于 25-5月 -23

启动 Control File and SPFILE Autobackup 于 25-5月 -23
段 handle=D:\ORACLE\BAKUP\RMANBAK\CONTROLFILE\CONSP_C-1662702591-20230525-00 comment=NONE
完成 Control File and SPFILE Autobackup 于 25-5月 -23

RMAN>
```

![](14-1rman数据备份.jpg)

![数据备份14-2](数据备份14-2.jpg)

### 4.2 数据的还原

```
RMAN> run{
2>   restore controlfile from 'D:\oracle\bakup\rmanbak\controlfile\CONSP_C-1662702591-20230505-07';
3>   sql'alter database mount';
4>   restore database;
5>   recover database;
6>   sql'alter database open resetlogs';
7>  }

启动 restore 于 27-4月 -23
使用通道 ORA_DISK_1

通道 ORA_DISK_1: 正在还原控制文件
通道 ORA_DISK_1: 还原完成, 用时: 00:00:01
输出文件名=D:\APP\JIANG\ORADATA\ORCL\CONTROL01.CTL
输出文件名=D:\APP\JIANG\FLASH_RECOVERY_AREA\ORCL\CONTROL01.CTL
完成 restore 于 27-4月 -23

sql 语句: alter database mount
释放的通道: ORA_DISK_1

启动 restore 于 27-4月 -23
启动 implicit crosscheck backup 于 27-4月 -23
分配的通道: ORA_DISK_1
通道 ORA_DISK_1: SID=189 设备类型=DISK
已交叉检验的 12 对象
完成 implicit crosscheck backup 于 27-4月 -23

启动 implicit crosscheck copy 于 27-4月 -23
使用通道 ORA_DISK_1
已交叉检验的 7 对象
完成 implicit crosscheck copy 于 27-4月 -23

搜索恢复区中的所有文件
正在编制文件目录...
没有为文件编制目录

使用通道 ORA_DISK_1

通道 ORA_DISK_1: 正在还原数据文件00001
输入数据文件副本 RECID=6 STAMP=1135267426 文件名=D:\ORACLE\BAKUP\RMANBAK\DB\FULL_DB_ORCL2N1QLJJ1.BAK
数据文件 00001 的还原目标: D:\APP\JIANG\ORADATA\ORCL\SYSTEM01.DBF
通道 ORA_DISK_1: 已复制数据文件 00001 的数据文件副本
输出文件名=D:\APP\JIANG\ORADATA\ORCL\SYSTEM01.DBF RECID=0 STAMP=0
通道 ORA_DISK_1: 正在还原数据文件00002
输入数据文件副本 RECID=7 STAMP=1135267427 文件名=D:\ORACLE\BAKUP\RMANBAK\DB\FULL_DB_ORCL2O1QLJJ2.BAK
数据文件 00002 的还原目标: D:\APP\JIANG\ORADATA\ORCL\SYSAUX01.DBF
通道 ORA_DISK_1: 已复制数据文件 00002 的数据文件副本
输出文件名=D:\APP\JIANG\ORADATA\ORCL\SYSAUX01.DBF RECID=0 STAMP=0
通道 ORA_DISK_1: 正在还原数据文件00003
输入数据文件副本 RECID=9 STAMP=1135267428 文件名=D:\ORACLE\BAKUP\RMANBAK\DB\FULL_DB_ORCL2Q1QLJJ4.BAK
数据文件 00003 的还原目标: D:\APP\JIANG\ORADATA\ORCL\UNDOTBS01.DBF
通道 ORA_DISK_1: 已复制数据文件 00003 的数据文件副本
输出文件名=D:\APP\JIANG\ORADATA\ORCL\UNDOTBS01.DBF RECID=0 STAMP=0
通道 ORA_DISK_1: 正在还原数据文件00004
输入数据文件副本 RECID=10 STAMP=1135267429 文件名=D:\ORACLE\BAKUP\RMANBAK\DB\FULL_DB_ORCL2R1QLJJ5.BAK
数据文件 00004 的还原目标: D:\APP\JIANG\ORADATA\ORCL\USERS01.DBF
通道 ORA_DISK_1: 已复制数据文件 00004 的数据文件副本
输出文件名=D:\APP\JIANG\ORADATA\ORCL\USERS01.DBF RECID=0 STAMP=0
通道 ORA_DISK_1: 正在还原数据文件00005
输入数据文件副本 RECID=8 STAMP=1135267427 文件名=D:\ORACLE\BAKUP\RMANBAK\DB\FULL_DB_ORCL2P1QLJJ3.BAK
数据文件 00005 的还原目标: D:\APP\JIANG\ORADATA\ORCL\EXAMPLE01.DBF
通道 ORA_DISK_1: 已复制数据文件 00005 的数据文件副本
输出文件名=D:\APP\JIANG\ORADATA\ORCL\EXAMPLE01.DBF RECID=0 STAMP=0
通道 ORA_DISK_1: 正在还原数据文件00006
输入数据文件副本 RECID=4 STAMP=1135267422 文件名=D:\ORACLE\BAKUP\RMANBAK\DB\FULL_DB_ORCL2L1QLJIR.BAK
数据文件 00006 的还原目标: D:\APP\JIANG\ORADATA\ORCL\AH.DBF
通道 ORA_DISK_1: 已复制数据文件 00006 的数据文件副本
输出文件名=D:\APP\JIANG\ORADATA\ORCL\AH.DBF RECID=0 STAMP=0
通道 ORA_DISK_1: 正在还原数据文件00007
输入数据文件副本 RECID=5 STAMP=1135267423 文件名=D:\ORACLE\BAKUP\RMANBAK\DB\FULL_DB_ORCL2M1QLJIU.BAK
数据文件 00007 的还原目标: D:\APP\JIANG\ORADATA\RMANSJWJ\TBS_RMAN.DBF
通道 ORA_DISK_1: 已复制数据文件 00007 的数据文件副本
输出文件名=D:\APP\JIANG\ORADATA\RMANSJWJ\TBS_RMAN.DBF RECID=0 STAMP=0
完成 restore 于 27-4月 -23

启动 recover 于 27-4月 -23
使用通道 ORA_DISK_1

正在开始介质的恢复

线程 1 序列 28 的归档日志已作为文件 D:\APP\JIANG\ORADATA\ORCL\REDO01.LOG 存在于磁盘上
归档日志文件名=D:\APP\JIANG\ORADATA\ORCL\REDO01.LOG 线程=1 序列=28
介质恢复完成, 用时: 00:00:00
完成 recover 于 27-4月 -23

sql 语句: alter database open resetlogs
```

## 五.总结

1. 表空间和表的设计方案：
   - 创建了两个表空间：`data_tablespace`用于存储数据文件，`index_tablespace`用于存储索引文件。
   - 创建了四张表：`PRODUCTS`、`CUSTOMERS`、`ORDERS`和`ORDERTAIL`。
   - 使用合适的字段定义了每张表的结构，包括主键、外键和索引的设置。
2. 用户管理方案：
   - 创建了两个用户：`admin_cust`和`normal_cust`。
   - `admin_cust`用户拥有对所有表的完全访问权限，可以进行读写操作。
   - `normal_cust`用户只具有查询权限，只能对表进行读取操作。
3. PL/SQL设计方案：
   - 创建了一个名为`cust_pkg`的包。
   - 在该包中定义了一个存储过程`cust_order`，接受客户号作为输入参数，并输出该客户的相关信息和订单详情。
4. 备份方案设计：
   - 使用RMAN（Recovery Manager）工具进行数据库备份。
   - 切换数据库到归档日志模式，并设置相应的RMAN配置参数。
   - 定义备份脚本，包括自动备份控制文件、备份集的保留策略、备份优化和删除过时备份集等设置。
   - 数据恢复和还原操作可使用RMAN工具进行，根据备份的情况选择合适的恢复策略。

综上所述，我设计了一个基于Oracle数据库的商品销售系统的数据库方案。该方案包括了表空间和表的设计、用户管理方案、PL/SQL存储过程和函数的设计，以及数据库备份方案。这些设计能够满足商品销售系统的需求，并提供了数据安全性和可恢复性的保障。